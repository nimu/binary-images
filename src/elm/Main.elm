port module Main exposing (Flags, Model, Msg(..), init, main, subscriptions, update, view)

-- Imports ---------------------------------------------------------------------

import Binary
import Browser
import Browser.Events exposing (onKeyPress)
import Json.Decode as Decode
import Json.Encode exposing (Value, string)
import Svg.Styled exposing (Svg, rect, svg)
import Svg.Styled.Attributes exposing (..)


port jsonConsole : Value -> Cmd msg



-- Main ------------------------------------------------------------------------


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view >> Svg.Styled.toUnstyled
        , subscriptions = subscriptions
        }



-- Model -----------------------------------------------------------------------


type alias Flags =
    ()


type alias Model =
    { width : Int
    , height : Int
    , data : List Int
    }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { width = 8
      , height = 8
      , data = []
      }
    , Cmd.none
    )



-- Update ----------------------------------------------------------------------


type Msg
    = AddBlack
    | AddWhite
    | Delete
    | Noop


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddBlack ->
            if List.length model.data < model.width * model.height then
                let
                    newData =
                        model.data ++ [ 1 ]
                in
                ( { model | data = newData }, logToConsole newData )

            else
                ( model, Cmd.none )

        AddWhite ->
            if List.length model.data < model.width * model.height then
                let
                    newData =
                        model.data ++ [ 0 ]
                in
                ( { model | data = newData }, logToConsole newData )

            else
                ( model, Cmd.none )

        Delete ->
            let
                n =
                    List.length model.data
            in
            if n == 0 then
                ( model, Cmd.none )

            else
                let
                    newData =
                        model.data |> List.reverse |> List.drop 1 |> List.reverse
                in
                ( { model | data = newData }, logToConsole newData )

        Noop ->
            ( model, Cmd.none )


logToConsole : List Int -> Cmd msg
logToConsole data =
    if modBy 4 (List.length data) == 0 then
        jsonConsole (encodeModel data)

    else
        Cmd.none


encodeModel : List Int -> Value
encodeModel data =
    let
        addSpaceEveryTwoChars : Char -> String -> String
        addSpaceEveryTwoChars char str =
            if modBy 3 (String.length str) == 2 then
                str ++ " " ++ String.fromChar char

            else
                str ++ String.fromChar char
    in
    data
        |> Binary.fromIntegers
        |> Binary.toHex
        |> String.foldl addSpaceEveryTwoChars ""
        |> Json.Encode.string



-- View ------------------------------------------------------------------------


view : Model -> Svg Msg
view model =
    svg
        [ viewBox "0 0 400 400"
        , width "98vw"
        , height "98vh"
        , fontSize "1.5em"
        ]
    <|
        viewPixelData model


viewPixelData : Model -> List (Svg msg)
viewPixelData { height, width, data } =
    drawGrid width
        ++ drawData width data


squareSize =
    50


createSquare : Int -> Int -> String -> Svg msg
createSquare row col color =
    rect
        [ width (String.fromInt squareSize)
        , height (String.fromInt squareSize)
        , fill color
        , x (String.fromInt (col * squareSize))
        , y (String.fromInt (row * squareSize))
        , strokeWidth "1"
        , stroke "darkgray"
        ]
        []


drawGrid : Int -> List (Svg msg)
drawGrid gridSize =
    let
        createRow rowIdx =
            List.indexedMap (\colIdx _ -> createSquare rowIdx colIdx "lightgray") (List.range 0 (gridSize - 1))
    in
    List.indexedMap (\rowIdx _ -> createRow rowIdx) (List.range 0 (gridSize - 1)) |> List.concat


drawData : Int -> List Int -> List (Svg msg)
drawData gridSize data =
    let
        drawDataPoint : Int -> Int -> Svg msg
        drawDataPoint idx bit =
            let
                color =
                    if bit == 1 then
                        "black"

                    else
                        "white"
            in
            createSquare (idx // gridSize) (modBy gridSize idx) color
    in
    List.indexedMap drawDataPoint data



-- Subscriptions ---------------------------------------------------------------


subscriptions : Model -> Sub Msg
subscriptions _ =
    onKeyPress keyDecoder


keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map keyToMsg (Decode.field "key" Decode.string)


keyToMsg : String -> Msg
keyToMsg string =
    case string of
        "d" ->
            Delete

        "backspace" ->
            Delete

        "0" ->
            AddWhite

        "r" ->
            AddWhite

        "b" ->
            AddBlack

        "1" ->
            AddBlack

        _ ->
            Noop
